library(relatorios)
source("code/R/lib/set_criterios_desp.R")

#====================================================================
# preparacao dos dados
base <- reest::ler_exec_desp("data-raw/exec_desp.xlsx")
# base <- rbind(execucao::exec_desp[ANO %in% 2015:2016], base)

#====================================================================
# aplicacao de criterios
base$BASE <- "EXEC"
base <- adiciona_desc_desp(base)
base <- add_criterios_desp(base)
base[, DATA_EXEC := paste(ANO, formatC(MES_COD, width = 2, flag = "0"), "01", sep = "-")]
#====================================================================
# exportacao dos dados
relatorios::exporta_csv(base, "data/exec_desp.csv", row.names = FALSE)
