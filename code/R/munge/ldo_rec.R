library(relatorios);library(data.table)
source("code/R/lib/set_criterios_rec.R")

#====================================================================
# preparacao dos dados
base <- execucao::ldo_rec

#====================================================================
# aplicacao de criterios
base$BASE <- "LDO"

# coluna ANO nao armazena ano que deve ser usado para fins de criterios
base[, tmp := ANO]
base[, ANO := ANO_REF - 1]

base <- adiciona_desc_rec(base)
base <- add_criterios_rec(base)
base <- add_criteiro_rec_fonte(base)

base[, ANO := tmp]
base[, tmp := NULL]

#====================================================================
# exportacao dos dados
relatorios::exporta_csv(base, "data/ldo_rec.csv", row.names = FALSE)
