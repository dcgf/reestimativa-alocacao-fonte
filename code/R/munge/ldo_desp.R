library(relatorios)
source("code/R/lib/set_criterios_desp.R")

#====================================================================
# preparacao dos dados
base <- execucao::ldo_desp

#====================================================================
# aplicacao de criterios
base$BASE <- "LDO"

# coluna ANO nao armazena ano que deve ser usado para fins de criterios
base[, tmp := ANO]
base[, ANO := ANO_REF - 1]

base <- adiciona_desc_desp(base)
base <- add_criterios_desp(base)

base[, ANO := tmp]
base[, tmp := NULL]

#====================================================================
# exportacao dos dados
relatorios::exporta_csv(base, "data/ldo_desp.csv", row.names = FALSE)
