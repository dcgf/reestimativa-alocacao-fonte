' ------------------------------------------
' Inicializacao de variaveis de sistema
Dim fso, wshShell

Set fso = CreateObject("Scripting.FileSystemObject")
Set wshShell = CreateObject("WScript.Shell")
' ------------------------------------------
' Inicializacao de variaveis de usuario
old_drive = fso.BuildPath(wshShell.CurrentDirectory, "\")
old_folder = fso.BuildPath(new_drive, "\data-raw\")
old_file = Wscript.Arguments.Item(0)
old_path = fso.BuildPath(fso.BuildPath(old_drive, old_folder), old_file)

new_drive = fso.BuildPath(wshShell.CurrentDirectory, "\")
new_folder = fso.BuildPath(new_drive, "\data-raw\")
new_file = Wscript.Arguments.Item(1)
' ------------------------------------------
' Inicializacao das variaveis do excel e operações
Dim excel, workbook 

WScript.Echo "Copiando " & old_file & " em data-raw\" & new_file & "..."

Set excel = CreateObject("Excel.Application")
excel.Application.DisplayAlerts = False

Set workbook = excel.Workbooks.Open(fso.GetFile(old_path))
workbook.Worksheets(1).Cells.ClearFormats
workbook.SaveAs new_folder & new_file, 51
workbook.Close

excel.Application.DisplayAlerts = True
excel.Application.Quit 
' ------------------------------------------
' Finalização
Set fso = Nothing
Set workbook = Nothing    
Set excel = Nothing
Set wshShell = Nothing

wScript.Quit

