#====================================================================
# Transferencias aos municipios

# Fonte 20
x <- base$rec[FONTE_COD == 20, 
              sum(VL_REC)]

y <- base$desp[ACAO_COD == 7844 & FONTE_COD == 20, 
               sum(VL_DESP)] 

test_equal(x, y,
           descricao = "Fonte 20",
           contexto = "Transferências Constitucionais e Legais")

# Fonte 51
x <- base$rec[RECEITA_COD == 1721011302, 
              sum(VL_REC)]

y <- base$desp[ACAO_COD == 7844 & FONTE_COD == 51, 
               sum(VL_DESP)] 

test_equal(x, y,
           descricao = "Fonte 51",
           contexto = "Transferências Constitucionais e Legais")
