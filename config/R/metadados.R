# As seguintes variaveis do Armazem sao usadas na aba metadados
# BASE = "Base XYZ"
# DATA_ATUALIZACAO =Data�ltimaExecu��o()
# ANO_REF = FiltroDeRelat�rio([Ano de Exerc�cio]) 
# MES_REF = M�x([M�s - Num�rico]) 

config <- yaml::yaml.load_file("config/config.yaml")
stem <- names(config$munge_base)[as.logical(config$munge_base)]
stem <- stem[grepl("^exec_", stem)]

if(length(stem) > 0) {
  files <- paste0("data-raw/", stem, ".xlsx")
  metadados <- Map(readxl::read_excel, files, "metadados")
  metadados <- Reduce(rbind, metadados)
  metadados <- metadados[!is.na(metadados$BASE),]
} else {
  metadados <- data.frame(BASE = "N�o existem bases da execu��o or�ament�ria")
}

relatorios::exporta_csv(metadados, file = "data/metadados.csv", row.names = FALSE)
