config <- yaml::yaml.load_file("config/config.yaml")
stem <- names(config$munge_base)[as.logical(config$munge_base)]
stem <- stem[grepl("^exec_", stem)]

if(length(stem) > 0) {
  files <- paste0("data-raw/", stem, ".xlsx")
} else {
  files <- ""
}

write(files, file = stdout())
