config <- yaml::yaml.load_file("config/config.yaml")

if(config$test$run_tests == TRUE) {
  stem <- names(config$test$tests)[as.logical(config$test$tests)]
  stem <- c(config$test$base, stem)
  files <- paste0("tests/test/test_", stem, ".R")
} else{
  files <- ""
}

write(files, file = stdout())
