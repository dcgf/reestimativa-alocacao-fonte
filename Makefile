.PHONY: help build munge test clean

#====================================================================
# gera lista de arquivos com base em config.yaml

MUNGE_FILES := $(shell Rscript config/R/generate_munge_files.R 2> logs/log.Rout)
EXEC_FILES := $(shell Rscript config/R/generate_exec_files.R 2> logs/log.Rout)
TEST_FILES := $(shell Rscript config/R/generate_test_files.R 2> logs/log.Rout)
LIB := $(wildcard code/R/lib/*.R)
CONFIG := config/config.yaml
STEM_QVW := $(basename $(wildcard *.qvw))
DEPLOY_PATH := $(shell Rscript config/R/get_deploy_path.R 2> logs/log.Rout)

#====================================================================
# determina bases que serao copiadas na execucao dos targets copy com base em config.yaml

TARGETS_COPY_EXEC := $(shell Rscript config/R/get_targets_copy.R exec 2> logs/log.Rout)
TARGETS_COPY_REEST := $(shell Rscript config/R/get_targets_copy.R reest 2> logs/log.Rout)
TARGETS_COPY_LDO := $(shell Rscript config/R/get_targets_copy.R ldo 2> logs/log.Rout)
TARGETS_COPY_LOA := $(shell Rscript config/R/get_targets_copy.R loa 2> logs/log.Rout)

#====================================================================
# RECURSIVELY EXPANDED VARIABLES

COMMIT=`cat logs/commit.txt`
DATE=`date +%Y-%m-%d`

#====================================================================
# PHONY TARGETS

help: 
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'

deploy: build ## Atualiza o qlikview, faz commit da alteração e disponibiliza o .qvw na rede da SCPPO
	@echo "Atualizando Qlikview..."
	@qv /r $(STEM_QVW).qvw
	@echo "Atualizando GitLab..."
	@echo "-----------------------------------------------------"
	@git add -u
	@git commit -m "Disponibiliza app qvw na pasta da SCPPO"
	@git push origin master
	@git rev-parse --short HEAD > logs/commit.txt
	@echo "-----------------------------------------------------"
	@cp $(STEM_QVW).qvw $(PATH_SCPPO)/$(DEPLOY_PATH)/$(STEM_QVW)_$(DATE)_$(COMMIT).qvw
	@echo "Arquivo $(STEM_QVW)_$(DATE)_$(COMMIT).qvw criado com sucesso em $(PATH_SCPPO)/$(DEPLOY_PATH)/"
	@echo
	@echo "Deploy completo."
	@echo "-----------------------------------------------------"

build: munge data/metadados.csv data/testes.csv code/qvw/load_bases.qvs ## Atualiza make munge, testes de consistência e metadados das bases execução
	@echo
	@echo "Build completo."
	@echo "-----------------------------------------------------"

munge: $(MUNGE_FILES) ## Atualiza as bases presentes em data/ que possuem script correspondente em code/R/munge/
	@echo
	@echo "Munging completo."
	@echo "-----------------------------------------------------"

#====================================================================
# TARGETS

$(MUNGE_FILES): data/%.csv: code/R/munge/%.R data-raw/%.xlsx $(CONFIG) $(LIB)
	@Rscript config/R/check_pkg_version.R 2> logs/log.Rout
	@echo "Atualizando $*..."
	@Rscript $< 2> logs/log.Rout

data/metadados.csv: config/R/metadados.R $(EXEC_FILES) $(CONFIG) $(LIB)
	@echo "Atualizando metadados..."
	@Rscript $< 2> logs/log.Rout

data/testes.csv: tests/test.R $(TEST_FILES) $(MUNGE_FILES) $(CONFIG) $(LIB)
	@echo "Atualizando testes..."
	@Rscript $< 2> logs/log.Rout

code/qvw/load_bases.qvs: config/R/load_bases.R $(CONFIG)
	@echo "Atualizando load_bases..."
	@Rscript $< 2> logs/log.Rout

#====================================================================
# HELPERS TARGETS

test: ## Atualiza os testes de consistência
	@echo "Atualizando testes..."
	@Rscript tests/test.R 2> logs/log.Rout

clean: clean_data clean_logs ## Remove o conteúdo das pastas data/ e logs/
	
clean_data:
	@echo "Removendo arquivos em data/..."
	@rm -f data/*.csv

clean_logs:
	@echo "Removendo arquivos em logs/..."
	@rm -f logs/*.Rout

copy: copy_exec copy_reest copy_loa copy_ldo ## Copia a base da receita e da despesa para sua pasta local

#====================================================================
# bases reest
copy_reest: $(TARGETS_COPY_REEST)

copy_reest_rec:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

copy_reest_desp:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

#====================================================================
# bases loa
copy_loa: $(TARGETS_COPY_LOA)

copy_loa_rec:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

copy_loa_desp:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

#====================================================================
# bases ldo
copy_ldo: $(TARGETS_COPY_LDO)

copy_ldo_rec:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

copy_ldo_desp:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

#====================================================================
# bases exec
copy_exec: $(TARGETS_COPY_EXEC)

copy_exec_rec:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

copy_exec_desp:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

copy_exec_rp:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

copy_exec_rp_folha:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout

copy_exec_suplementacao:
	@Rscript config/R/copy.R $@ 2> logs/log.Rout