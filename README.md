# Project Template v0.1.1

Este diretório visa documentar o diretório padronizado que é utilizado para os projetos da DCGF que de alguma forma envolvem a construção de Cenários Fiscais.

O principal objetivo para adoção de um diretório padrão é a disseminação de melhores práticas em relação as ferramentas utilizadas no âmbito da DCGF, e, ao mesmo tempo, a redução do trabalho repetitivo na inicialização de um novo projeto.

A documentação completa da estrutura está disponível em http://dcgf.gitlab.io/. 

## Instalação e Configuração do Ambiente

Antes de iniciar um novo projeto, é necessário garantir que todos os aplicativos necessários estão devidamente instalados e configurados. Um guia completo de instalação e configuração do ambiente está disponível [aqui](http://dcgf.gitlab.io/config-ambiente.html). Os problemas frequentes relacionados a configuração estão descritos [aqui](http://dcgf.gitlab.io/config-ambiente.html#faq).

## Inicialização do Projeto

Os passos para inicialização de um novo projeto são:

1. Faça o download deste repositório para seu computador [aqui](https://github.com/dcgf/project-template/archive/master.zip).

2. Altere o nome da pasta, do aplicativo qlikview (`.qvw`), e do projeto rstudio (`.Rproj`) para o nome do seu projeto/demanda. Os nomes dos projetos serão padronizados com `projeto_<ANO>` ou `projeto-composto_<ANO>`. Alguns exemplos são `reestimativa_2015`, `orcamento_2016`, `ldo_2017`, e `afd_2015`. As demandas devem ser nomeadas como `<ANO>-<MES>_demanda` ou `<ANO>-<MES>_demanda-composta`. Alguns exemplos são `2016-02_evolucao-fiscal`, `2015-12_fluxo-caixa` e `2016-06_dre`. Sempre utilize caixa baixa nos nomes e - para separar nomes compostos. A racionalidade é que projetos possuem uma durabilidade e padronização maior, e, portanto, são pesquisados com base no nome. Já demandas possuem natureza transitória, e, portanto, usualmente são pesquisadas com base na data que foram elaboradas.

3. Crie um novo projeto no [GitLab](https://gitlab.com). Você deve usar o mesmo nome do seu projeto, e, via de regra, o projeto deve pertencer a DCGF e não ao seu usuário e deve ser criado como um repositório privado. 

4. Na pasta local, inicialize um novo repositório git, faça um commit inicial, e faça o push das informações. Os comandos abaixo fazem esses passos. Importante ressaltar que o protocolo utilizado para transferência do repositório deve ser `https`. Isso implica que a `url` do repositório deve iniciar com `https://`

	```
	git init
	git add .
    git commit -m "Initial commit"
    git remote add origin <https://remote-url>
	git push -u origin master
	```

5. O arquivo `config.yaml` controla quais bases serão processadas e quais testes serão realizados. Faça as alterações nesse arquivo necessárias para atender o projeto concreto.

6. A pasta `data-raw/` possui arquivos *dummy* para os dados que estão disponíveis no pacote `execucao`. No entanto, é necessário inserir as bases relativas a execução orçamentária e a reestimativa. Você pode fazer isso utilizado os targets `make copy_reest` e `make copy_exec`.  Note que no caso do `copy_exec` é necessário uma pasta padronizada em `~/Downloads/execucao/` que armazena as bases enviadas diariamente pelo Armazém SIAFI.

7. Todas os demais fluxos do Template podem ser controlados pela linha de comando com o uso de targets do `Makefile`. A ajuda do mesmo pode ser acessada digitando-se `make` na linha de comando. O principal target é o `make build` que gera as bases em `data/` que posteriormente podem ser carregadas no aplicativo qlikview.


## Boas Práticas no uso do Template

Algumas boas práticas para uso do Project Template que facilitam a coordenação entre os servidores são:

- Sempre faça um *"Initial Commit"* antes de inserir alterações no diretório padronizado. Esse commit facilita a identificação das especificidades de um projeto para servidores que não participaram originalmente da elaboração do mesmo.

- Após o *"Initial Commit"* evite mesclar alterações em arquivos texto e binários nos commits. Essa distinção visa facilitar o processo de resolução de conflitos de merge que irão ocorrer. Atualmente os únicos arquivos binários do Project Template são as bases `.xslx` em `data-raw/` e o aplicativo qlikview (`.qvs`).